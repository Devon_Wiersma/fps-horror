using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;

public class AudioSourceInstance : MonoBehaviour
{


    AudioSource audio;
    bool checkDestroy;
    public bool isNote;

    public AudioClip[] noteClips;
    public string[] noteSubtitles;
    private int currentNoteClipIndex;

    AudioMixer StandardMixer;
    AudioMixer AlleyReverbSFXMixer;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();

        if (isNote)
        {
            audio.clip = noteClips[currentNoteClipIndex];
            GameObject.Find("Subtitles").GetComponent<TextMeshProUGUI>().text = noteSubtitles[currentNoteClipIndex];
            audio.Play();
            StartCoroutine(DestroyCheckDelay());
        }
        else
        {
            audio.outputAudioMixerGroup = GameObject.Find("Audio Handler").GetComponent<AudioHandler>().activeMixer;

        }


    }

    private void Update()
    {
        if (isNote)
        {
            if (audio.isPlaying != true && currentNoteClipIndex != noteClips.Length - 1)
            {

                currentNoteClipIndex++;
                GameObject.Find("Subtitles").GetComponent<TextMeshProUGUI>().text = noteSubtitles[currentNoteClipIndex];
                audio.clip = noteClips[currentNoteClipIndex];
                audio.Play();
            }
            else
            {


            }
        }

    }

    IEnumerator DestroyCheckDelay()
    {
        yield return new WaitForSeconds(1);
        if (audio.isPlaying != true)
        {
            if (isNote)
            {
                GameObject.Find("Subtitles").GetComponent<TextMeshProUGUI>().text = "";
            }
            Destroy(gameObject);

        }
        else
        {
            StartCoroutine(DestroyCheckDelay());
        }
    }
}

