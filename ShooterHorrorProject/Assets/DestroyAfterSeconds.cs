using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterSeconds : MonoBehaviour
{
    public float timeToWait;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("WaitForSeconds");
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator WaitForSeconds()
    {

        yield return new WaitForSeconds(timeToWait);
        Destroy(gameObject);
    }
}
