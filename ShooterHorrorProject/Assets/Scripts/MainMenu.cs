using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private float camRotationSpeed = 5;

    public GameObject optionsPanel;
    public GameObject creditsPanel;

    public GameObject specialMessagePanel;

    public GameObject newGameWarning;
    public GameObject resumeButton;
    public GameObject specialMessageButton;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;




        if (SaveSystem.HasKey("BeatGame"))
        {
            specialMessagePanel.SetActive(true);
            specialMessageButton.SetActive(true);
        }
        else
        {
            specialMessagePanel.SetActive(false);
            specialMessageButton.SetActive(false);
        }

        //REMEMBER: WE HAVE TO GET SCENE NAMES FROM INDEXES THIS WAY BECAUSE UNITY SUCKS
        string pathToScene = SceneUtility.GetScenePathByBuildIndex(1);
        string firstScene = System.IO.Path.GetFileNameWithoutExtension(pathToScene);

        if (SaveSystem.HasKey("LastSceneLoaded"))
        {
            resumeButton.SetActive(true);
        }
        else
        {
            resumeButton.SetActive(false);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
    }

    public void TogglePanel(GameObject panelToToggle)
    {
        if (panelToToggle.activeInHierarchy == true)
        {
            DisableAllPanels();
        }
        else
        {
            DisableAllPanels();
            panelToToggle.SetActive(true);
        }
    }

    void DisableAllPanels()
    {
        creditsPanel.SetActive(false);
        optionsPanel.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ResumeGame()
    {

        if (SaveSystem.GetInt("LastSceneLoaded") == 5 || SaveSystem.GetInt("LastSceneLoaded") == 6 || SaveSystem.GetInt("LastSceneLoaded") == 7)
        {
            FindObjectOfType<SceneLoader>().LoadScene(4);
        }
        else
        {
            //load the scene from the last stored scene that was loaded
            //this is saved in the SceneLoader script
            FindObjectOfType<SceneLoader>().LoadScene(SaveSystem.GetInt("LastSceneLoaded"));
        }

        // SaveSystem.SetInt("LastSceneLoaded", SceneManager.GetActiveScene().buildIndex)
    }

    public void NewGameCheck()
    {
        //REMEMBER: WE HAVE TO GET SCENE NAMES FROM INDEXES THIS WAY BECAUSE UNITY SUCKS
        string pathToScene = SceneUtility.GetScenePathByBuildIndex(1);
        string firstScene = System.IO.Path.GetFileNameWithoutExtension(pathToScene);

        if (SaveSystem.HasKey(firstScene))
        {
            newGameWarning.SetActive(true);
            print("Showing warning");
        }
        else
        {
            FindObjectOfType<SceneLoader>().LoadScene(1);
            print("Starting new game for first time user");
        }
    }

    public void WipeAndNewGame()
    {
        //  FindObjectOfType<SaveLoader>().WipeAllData();
        FindObjectOfType<SceneLoader>().LoadScene(1);
        print("Wiping data and starting new game");
    }



    public void OpenDiscordLink()
    {
        Application.OpenURL("https://discord.com/invite/xJTrVwa2g4");
    }
    public void OpenTwitterLink()
    {
        Application.OpenURL("https://twitter.com/Devon_Wiersma");
    }
    public void OpenItchioLink()
    {
        Application.OpenURL("https://devonwiersma.itch.io/");
    }
    public void OpenSteamLink()
    {
        Application.OpenURL("https://store.steampowered.com/search/?developer=Devon%20Wiersma");
    }

}
