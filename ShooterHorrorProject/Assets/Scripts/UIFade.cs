using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIFade : MonoBehaviour
{
    public float fadeRate;
    public float fadeInDelayTime;
    public float fadeOutDelayTime;
    public int fadeInTime;
    public int fadeOutTime;
    public bool fadeOutOnStart;
    public bool fadeInOnStart;
    bool fadingOut;
    bool fadingIn;

    public bool fadeOutAfterFadeIn;

    public CanvasGroup canGroupToFade;

    // Use this for initialization
    void Start()
    {
        if (fadeOutOnStart)
        {
            FadeOut(fadeOutTime);
        }
        if (fadeInOnStart)
        {
            FadeIn(fadeInTime);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (fadingIn)
            canGroupToFade.alpha += fadeRate;

        if (fadingOut)
            canGroupToFade.alpha -= fadeRate;
    }

    public void FadeOut(int seconds)
    {
        StartCoroutine("FadeOutTimed", seconds);

    }

    public void FadeIn(int seconds)
    {
        StartCoroutine("FadeInTimed", seconds);
    }

    IEnumerator FadeInTimed(int fadeTime)
    {

        yield return new WaitForSeconds(fadeInDelayTime);
        fadingIn = true;

        yield return new WaitForSeconds(fadeInTime);
        fadingIn = false;
        if (fadeOutAfterFadeIn)
            FadeOut(fadeOutTime);
    }

    IEnumerator FadeOutTimed(int fadeTime)
    {

        yield return new WaitForSeconds(fadeOutDelayTime);
        fadingOut = true;
        yield return new WaitForSeconds(fadeOutTime);
        fadingOut = false;
    }
}
