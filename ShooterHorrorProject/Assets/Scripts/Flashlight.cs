using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    public float defaultIntensity;
    Light flashlight;

    public bool flashlightFlicker;

    // Start is called before the first frame update
    void Start()
    {
        flashlight = GetComponent<Light>();
        defaultIntensity = flashlight.intensity;
    }

    // Update is called once per frame
    void Update()
    {
        if (flashlightFlicker)
        {
            flashlight.intensity = Random.Range(defaultIntensity - 2f, defaultIntensity + 2f);
        }
        else
        {
            flashlight.intensity = defaultIntensity;
        }

    }
}
