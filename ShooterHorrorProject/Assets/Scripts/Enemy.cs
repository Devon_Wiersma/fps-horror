using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public NavMeshAgent navAgent;
    public AudioSource soundSource;
    public Transform mesh;
    public Transform head;
    public GameObject eyes;
    public Light eyeLight;

    GameObject player;
    public GameObject overrideTarget;

    public Animator animator;

    public int maxHealth;
    public int currentHealth;
    public int crawlHealth;
    public int deathHealth;

    bool enemyActivated = false;
    bool playerNearby;
    public bool canRegenHealth;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");

        soundSource.Stop();
        eyes.SetActive(false);
        eyeLight.enabled = false;
        animator.speed = 0;
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        Mathf.Clamp(currentHealth, 0, maxHealth);
        mesh.rotation = Quaternion.Euler(0, mesh.transform.eulerAngles.y, mesh.transform.eulerAngles.z);

        if (currentHealth <= crawlHealth)
        {
            if (animator.GetBool("Crawling") == false)
            {
                animator.SetBool("Crawling", true);
                StartCoroutine("Heal");
            }
        }

        if (currentHealth <= deathHealth)
        {
            playerNearby = false;
            GameObject.Find("Flashlight").GetComponent<Flashlight>().flashlightFlicker = false;
            GameObject.Find("Main Camera").GetComponent<MainCameraVFXHandler>().glitch = false;
            Destroy(navAgent.gameObject);
        }

    }

    private void LateUpdate()
    {

        if (overrideTarget != null)
        {
            if (!enemyActivated)
            {
                ActivateEnemy(overrideTarget.transform);
                head.LookAt(overrideTarget.transform.position);
                navAgent.SetDestination(overrideTarget.transform.position);
                print("Enemy should be moving");
            }
        }
        else
        {
            if (enemyActivated)
            {
                head.LookAt(player.transform.position);
                navAgent.SetDestination(player.transform.position);
            }
        }


    }

    public void UpdateOoverrideTarget(GameObject newTarget)
    {
        overrideTarget = newTarget;
        
    }

    IEnumerator Heal()
    {
        currentHealth += 3;
        yield return new WaitForSeconds(1);
        if (currentHealth <= maxHealth)
        {
            if (canRegenHealth)
                StartCoroutine("Heal");
        }
        else
        {
            animator.SetBool("Crawling", false);
        }
    }

    public void ActivateEnemy(Transform target)
    {
        enemyActivated = true;
        eyes.SetActive(true);

        eyeLight.enabled = true;
        animator.speed = 1;
        soundSource.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            ActivateEnemy(other.transform);
            print("player detected");
            playerNearby = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameObject.Find("Flashlight").GetComponent<Flashlight>().flashlightFlicker = true;
            GameObject.Find("Main Camera").GetComponent<MainCameraVFXHandler>().glitch = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameObject.Find("Flashlight").GetComponent<Flashlight>().flashlightFlicker = false;
            GameObject.Find("Main Camera").GetComponent<MainCameraVFXHandler>().glitch = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        print("You DIED");
        if (collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<FPSControllerLPFP.FpsControllerLPFP>().gameEnd == true)
            {
                GameObject.FindObjectOfType<SceneLoader>().LoadScene(4);
            }
            else
            {
                GameObject.FindObjectOfType<DeathScreenUI>().ShowDeathUI();
                navAgent.isStopped = true;
                Destroy(gameObject);
            }

        }

    }
}
