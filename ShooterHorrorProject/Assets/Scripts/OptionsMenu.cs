using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;


public class OptionsMenu : MonoBehaviour
{
    public Slider musicSlider;
    public Slider SFXSlider;

    public AudioMixer MasterMixer;

    public TMPro.TMP_Dropdown qualityDropdown;
    public TMPro.TMP_Dropdown resolutionDropdown;

    public Resolution[] resolutions;
    public QualityLevel[] qualities;

    public GameObject optionsPanel;

    // Start is called before the first frame update
    private void Start()
    {
        //populate resolution menu with resolution options
        resolutions = Screen.resolutions;
        for (int i = 0; i < resolutions.Length; i++)
        {
            //doesn't actually do anything but in theory should cull duplicate options from resolution dropdown
            //
            if (!resolutionDropdown.GetComponent<TMP_Dropdown>().options.Contains(new TMP_Dropdown.OptionData(resolutions[i].ToString())))
            {
                resolutionDropdown.options[i].text = resolutions[i].ToString();
                resolutionDropdown.value = i;

                resolutionDropdown.options.Add(new TMP_Dropdown.OptionData(resolutionDropdown.options[i].text));
                resolutionDropdown.onValueChanged.AddListener(delegate { Screen.SetResolution(resolutions[resolutionDropdown.value].width, resolutions[resolutionDropdown.value].height, false); });
            }
        }

        if (PlayerPrefs.HasKey("QualityLevel"))
        {
            qualityDropdown.value = PlayerPrefs.GetInt("QualityLevel");
            SetQuality(PlayerPrefs.GetInt("QualityLevel"));
        }
        else
        {
            SetQuality(3);
        }

        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            print(PlayerPrefs.GetFloat("MusicVolume"));
            musicSlider.value = PlayerPrefs.GetFloat("MusicVolume");
            SetMusicVolumeLevel();
        }

        if (PlayerPrefs.HasKey("SFXVolume"))
        {
            print(PlayerPrefs.GetFloat("SFXVolume"));
            SFXSlider.value = PlayerPrefs.GetFloat("SFXVolume");
            SetSFXVolumeLevel();
        }


        SaveSettings();
        optionsPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
    }


    public void SetMusicVolumeLevel()
    {
        MasterMixer.SetFloat("MusicVolume", Mathf.Log10(musicSlider.value) * 20);
        PlayerPrefs.SetFloat("MusicVolume", musicSlider.value);
        SaveSettings();
    }

    public void SetSFXVolumeLevel()
    {
        MasterMixer.SetFloat("SFXVolume", Mathf.Log10(SFXSlider.value) * 20);
        PlayerPrefs.SetFloat("SFXVolume", SFXSlider.value);
        SaveSettings();
    }

    public void RestartLevel()
    {
        GameObject.FindObjectOfType<SceneLoader>().LoadScene(SceneManager.sceneCountInBuildSettings);
    }

    //public void SetFullscreen(bool isFullscreen)
    //{
    //    Screen.fullScreen = isFullscreen;
    //}


    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width,
                  resolution.height, Screen.fullScreen);


        SaveSettings();
    }

    public void FullscreenToggle()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }

    public void SetQuality(int qualityIndex)
    {

        // if the user is not using 
        //any of the presets
        if (qualityIndex != 6)
        {
            QualitySettings.SetQualityLevel(qualityIndex);
            PlayerPrefs.SetInt("QualityLevel", qualityIndex);


        }

        switch (qualityIndex)
        {
            case 0: // quality level - very low

                break;
            case 1: // quality level - low

                break;
            case 2: // quality level - medium

                break;
            case 3: // quality level - high

                break;
            case 4: // quality level - very high

                break;
            case 5: // quality level - ultra

                break;
        }

        qualityDropdown.value = qualityIndex;

        SaveSettings();
    }

    public void SaveSettings()
    {
        PlayerPrefs.Save();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
