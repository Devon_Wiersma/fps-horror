using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class GenericTrigger : MonoBehaviour
{
    public bool fireOnce;
    public bool fireOnLoad;

    public bool playerOnly = true;
    public bool enemyOnly;



    public UnityEvent OnEnter;
    public UnityEvent OnStay;
    public UnityEvent OnExit;

    private void Start()
    {
        if (fireOnLoad)
        {
            OnEnter.Invoke();
        }
        // gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.F1))
        {

        }
    }
    void OnTriggerEnter(Collider c)
    {
        if ((playerOnly && (c.gameObject.tag == "Player" || c.gameObject.tag == "GasTank")))
        {
            OnEnter.Invoke();
        }

        if (enemyOnly && c.gameObject.tag == "Enemy")
        {
            OnEnter.Invoke();

        }

        if (fireOnce)
        {

            gameObject.SetActive(false);
        }


    }
    void OnTriggerStay(Collider c) { OnStay.Invoke(); }
    void OnTriggerExit(Collider c) { OnExit.Invoke(); }
}
