using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    Transform playerTransform;

    // Start is called before the first frame update
    void Awake()
    {
        // playerTransform = GameObject.Find("Player").transform;
        //transform.eulerAngles = new Vector3(playerTransform.position.x, 0, playerTransform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Camera.main.transform.position, Vector3.up);
        // transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
    }
}
