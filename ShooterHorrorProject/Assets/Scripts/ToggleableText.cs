using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ToggleableText : MonoBehaviour
{
    public CanvasGroup canGroup;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            canGroup.alpha = 1;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            canGroup.alpha = 0;
        }
    }
}
