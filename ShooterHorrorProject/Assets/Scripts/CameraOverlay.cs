using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CameraOverlay : MonoBehaviour
{
    CanvasGroup canvasGroup;

    int intTime;
    int minutes;
    int seconds;

    int savedTime;

    public TextMeshProUGUI timer;

    // Start is called before the first frame update
    void Start()
    {
        LoadTimer();
        canvasGroup = GetComponent<CanvasGroup>();
        intTime += savedTime;
    }

    // Update is called once per frame
    void Update()
    {
        float newAlpha = Random.Range(0.5f, 0.55f);
        canvasGroup.alpha = newAlpha;

        UpdateTimer(Time.time);
    }

    void SaveTimer()
    {
        SaveSystem.SetInt("IntTime", intTime);
        SaveSystem.SetInt("Minutes", minutes);
        SaveSystem.SetInt("Seconds", seconds);
        SaveSystem.SaveToDisk();
    }

    void LoadTimer()
    {
        savedTime = SaveSystem.GetInt("IntTime");
        minutes = SaveSystem.GetInt("Minutes");
        seconds = SaveSystem.GetInt("Seconds");
    }

    private void OnDisable()
    {
        SaveTimer();
    }

    private void OnApplicationQuit()
    {
        SaveTimer();
    }

    void UpdateTimer(float time)
    {
        intTime = (int)time;
        intTime = intTime + savedTime;
        minutes = intTime / 60;
        seconds = intTime % 60;
        float fraction = time * 1000;
        fraction = (fraction % 1000);
        string timeText = string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction);
        timer.text = timeText;
    }
}
