using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleAnimationController : MonoBehaviour
{
    Animator animat;

    public bool idle1;
    public bool idle2;
    public bool idle3;
    public bool prone1;
    public bool takingCover1;
    public bool crawlAway;

    public float startDelay;
    public bool waitForTrigger;


    // Start is called before the first frame update
    void Start()
    {
        animat = GetComponent<Animator>();
        animat.speed = 0;
        if (idle1)
        {
            animat.SetBool("Idle1", true);
        }
        if (idle2)
        {
            animat.SetBool("Idle2", true);
        }
        if (idle3)
        {
            animat.SetBool("Idle3", true);
        }
        if (prone1)
        {
            animat.SetBool("Prone 1", true);
        }
        if (takingCover1)
        {
            animat.SetBool("Taking Cover 1", true);
        }
        if (crawlAway)
        {
            animat.SetBool("CrawlAway", true);
        }
        if (!waitForTrigger)
            StartCoroutine(StartAnimation());

    }

    public IEnumerator StartAnimation()
    {
        yield return new WaitForSeconds(startDelay);
        animat.speed = 1;
    }
}
