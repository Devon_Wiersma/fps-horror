using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class Interactible : MonoBehaviour
{
    public enum InteractibleType { Ammo, Note, Door, Key };
    public InteractibleType activeType;

    public bool canBeInteracted;
    public bool doorIsLocked;

    public CanvasGroup interactibleCanGroup;
    public TextMeshProUGUI interactibleText;

    public GameObject objectToDestroy;
    public GameObject objectToUnlock;

    public AudioClip audioClip;
    public AudioClip lockedDoorAudioClip;
    public GameObject audioInstance;

    public AudioClip[] NoteAudio;
    public string[] noteSubtitles;

    public string overrideText;

    public UnityEvent OnInteract;

    // Start is called before the first frame update
    private void Start()
    {
        //if (SaveSystem.HasKey(GetInstanceID().ToString()))
        //{
        //    if (SaveSystem.GetBool(GetInstanceID().ToString()) == true)
        //    {
        //        gameObject.SetActive(false);
        //        print("Object already interacted with; destroying on load!");
        //    }

        //}

    }

    // Update is called once per frame
    void Update()
    {
        if (canBeInteracted)
        {
            ActivateText();

            if (Input.GetKeyDown(KeyCode.E))
            {
                if (activeType == InteractibleType.Ammo)
                {
                    InstantiateAudioClip(audioClip);
                    GameObject.Find("Player").GetComponentInChildren<AutomaticGunScriptLPFP>().totalAmmo += 30;
                    OnInteract.Invoke();
                    SaveInteractState();
                    Destroy(gameObject);
                }

                if (activeType == InteractibleType.Note)
                {
                    InstantiateAudioClip(audioClip);
                    GetComponent<Interactible>().enabled = false;
                    OnInteract.Invoke();
                    SaveInteractState();
                    Destroy(gameObject);
                }

                if (activeType == InteractibleType.Door)
                {
                    if (!doorIsLocked)
                    {
                        interactibleText.text = "Open";
                        InstantiateAudioClip(audioClip);
                        OnInteract.Invoke();
                        SaveInteractState();
                        Destroy(gameObject);
                    }
                    else
                    {
                        interactibleText.text = "Locked";
                        InstantiateAudioClip(lockedDoorAudioClip);
                    }
                }

                if (activeType == InteractibleType.Key)
                {
                    interactibleText.text = "Pick up";
                    InstantiateAudioClip(audioClip);
                    objectToUnlock.GetComponent<Interactible>().doorIsLocked = false;
                    OnInteract.Invoke();
                    SaveInteractState();
                    Destroy(gameObject);
                }
            }
        }
        else
        {
            DeactivateText();
        }
    }

    public void InstantiateAudioClip(AudioClip audioClip)
    {
        if (activeType == InteractibleType.Note)
        {
            GameObject newSource = Instantiate(audioInstance, GameObject.Find("Player").transform);
            newSource.GetComponent<AudioSourceInstance>().isNote = true;
            newSource.GetComponent<AudioSourceInstance>().noteClips = NoteAudio;
            newSource.GetComponent<AudioSourceInstance>().noteSubtitles = noteSubtitles;
            //     newSource.GetComponent<AudioSource>().clip = audioClip;
            //      newSource.GetComponent<AudioSource>().Play();

        }
        else
        {
            GameObject newSource = Instantiate(audioInstance, GameObject.Find("Player").transform);
            newSource.GetComponent<AudioSource>().clip = audioClip;
            newSource.GetComponent<AudioSource>().Play();
        }

    }

    public void ActivateText()
    {
        if (activeType == InteractibleType.Door)
        {
            if (doorIsLocked)
            {
                interactibleText.text = "Locked";
            }
            else
            {
                interactibleText.text = "Open";
            }
        }

        if (overrideText != "")
        {
            interactibleText.text = overrideText;
        }

        interactibleCanGroup.alpha = 1;
    }
    public void DeactivateText()
    {
        interactibleCanGroup.alpha = 0;
    }

    public void SaveInteractState()
    {
        //SaveSystem.SetBool(GetInstanceID().ToString(), true);
        //SaveSystem.SaveToDisk();
    }
}
