using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using FPSControllerLPFP;

public class DeathScreenUI : MonoBehaviour
{
    public CanvasGroup deathCanvas;
    public FpsControllerLPFP player;
    public AutomaticGunScriptLPFP gunScript;

    public bool deathUIShowing;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F2))
        {
            ShowDeathUI();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (deathUIShowing)
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void ShowDeathUI()
    {
        deathUIShowing = true;
        FindObjectOfType<AudioHandler>().DisableAllAudio();

        deathCanvas.gameObject.SetActive(true);
        if (player != null)
        {
            player.enabled = false;
        }
        if (gunScript != null)
            gunScript.enabled = false;
        Cursor.lockState = CursorLockMode.None;
    }
}
