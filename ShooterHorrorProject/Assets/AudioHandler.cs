using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioHandler : MonoBehaviour
{
    public AudioSource[] allAudioSources;
    public AudioMixerGroup activeMixer;

    // Start is called before the first frame update
    void Start()
    {
        allAudioSources = FindObjectsOfType<AudioSource>();
        UpdateAudioFilters(activeMixer);
    }

    public void UpdateAudioFilters(AudioMixerGroup newAudioMixer)
    {
        foreach (AudioSource audioSource in allAudioSources)
        {
            if (audioSource != null)
            {
                audioSource.outputAudioMixerGroup = newAudioMixer;

            }
        }
        activeMixer = newAudioMixer;
    }

    public void DisableAllAudio()
    {
        foreach (AudioSource audioSource in allAudioSources)
        {
            if (audioSource != null)
            {
                audioSource.Stop();
            }
        }

    }
}
