using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneOffEvent : MonoBehaviour
{
    public AudioClip audioClip;


    bool fired = false;
    public bool onceOnly;
    public void PlaySoundAtPoint(Transform point)
    {
        if (fired && onceOnly)
        {
            print("Event already fired; skipping");
        }
        else
        {
            AudioSource.PlayClipAtPoint(audioClip, point.position);
        }


        fired = true;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
