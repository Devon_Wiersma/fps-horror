using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCheckpoint : MonoBehaviour
{
    public bool fireOnce;

    public GameObject blockerToEnable;

    // Start is called before the first frame update
    void Start()
    {

        //LoadUserPosition();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.F1))
        {
            LoadUserPosition();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //save the triggered ID and set this as last save
        SaveSystem.SetBool(GetInstanceID().ToString(), true);
        SaveSystem.SetInt("LastSave", GetInstanceID());

        //save the player's location + position for loading
        SaveSystem.SetVector3("Player Position", other.gameObject.transform.position);
        SaveSystem.SetVector3("Player Rotation", other.gameObject.transform.rotation.eulerAngles);
        FindObjectOfType<AutomaticGunScriptLPFP>().SaveAmmo();
    }

    void LoadUserPosition()
    {
        if (SaveSystem.GetBool(GetInstanceID().ToString()))
        {
            if (SaveSystem.GetInt("LastSave") == GetInstanceID())
            {
                GameObject.Find("Player").transform.position = SaveSystem.GetVector3("Player Position");
                GameObject.Find("Player").transform.rotation = Quaternion.Euler(SaveSystem.GetVector3("Player Rotation"));
                FindObjectOfType<AutomaticGunScriptLPFP>().LoadAmmo();
                if (blockerToEnable != null)
                    blockerToEnable.SetActive(true);
            }
        }
    }
}
