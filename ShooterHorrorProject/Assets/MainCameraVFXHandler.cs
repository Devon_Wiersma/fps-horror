using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraVFXHandler : MonoBehaviour
{
    private ShaderEffect_CRT crtShader;
    private ShaderEffect_Unsync unsyncShader;

    private float unsyncDefaultIntensity;
    private float unsyncGlitchIntensity = 0.1f;

    public bool glitch = false;

    AudioSource audSorce;
    // Start is called before the first frame update
    void Start()
    {
        crtShader = GetComponent<ShaderEffect_CRT>();
        unsyncShader = GetComponent<ShaderEffect_Unsync>();

        audSorce = GetComponent<AudioSource>();
        audSorce.Stop();

        unsyncDefaultIntensity = unsyncShader.speed;

        ToggleDesync();
    }


    // Update is called once per frame
    void Update()
    {
        ToggleDesync();
    }


    void ToggleDesync()
    {
        if (glitch)
        {
            StartCoroutine(DesyncTimer());
            if (!audSorce.isPlaying)
            {
                audSorce.Play();
            }

        }
        else
        {
            unsyncShader.speed = 0.0001f;

        }
    }

    IEnumerator DesyncTimer()
    {
        yield return new WaitForSeconds(1);
        float newGlitchIntensity = Random.Range(unsyncDefaultIntensity - unsyncGlitchIntensity, unsyncDefaultIntensity + unsyncGlitchIntensity);
        unsyncShader.speed = newGlitchIntensity;
    }


}
