using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRaycast : MonoBehaviour
{
    public Camera camera;

    public GameObject lookObject;

    void FixedUpdate()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit,2))
        {
            Transform objectHit = hit.transform;

            // Do something with the object that was hit by the raycast.
            if (hit.transform.GetComponent<Interactible>() != null)
            {
                lookObject = hit.transform.gameObject;
                lookObject.GetComponent<Interactible>().canBeInteracted = true;
            }
            else
            {
                if (lookObject != null)
                {
                    lookObject.GetComponent<Interactible>().canBeInteracted = false;
                    lookObject = null;
                }
            }

        }
    }
}
