using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float spawnTimer;

    public bool spawnBlocked;

    public GameObject enemyToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnEnemyTimer());
    }


    IEnumerator SpawnEnemyTimer()
    {
        yield return new WaitForSeconds(spawnTimer);

        if (!spawnBlocked)
        {
            Instantiate(enemyToSpawn, transform.position, transform.rotation);
            StartCoroutine(SpawnEnemyTimer());
        }
        else
        {
            StartCoroutine(SpawnEnemyTimer());
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            spawnBlocked = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            spawnBlocked = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            spawnBlocked = false;
        }
    }
}
