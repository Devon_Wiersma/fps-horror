using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class VideoController : MonoBehaviour
{
    VideoPlayer vidCont;

    // Start is called before the first frame update
    void Start()
    {
        vidCont = GetComponent<VideoPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!vidCont.isPlaying)
        {
            GameObject.FindObjectOfType<SceneLoader>().LoadScene(0);
        }
    }
}
